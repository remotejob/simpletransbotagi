# simpletransbotagi
conda create -n simpletranbotagi python=3.7 numpy scipy pandas
conda activate simpletranbotagi
conda install pytorch torchvision tqdm
pip install -U transformers
pip install -U scikit-learn 
pip install -U fastapi uvicorn

mkdir -p data/simpletransdata/best_model

scp -r data/simpletransdata/best_model/  root@159.203.67.26:/tmp/
scp  data/simpletransdata/intent.csv root@159.203.67.26:/tmp

systemctl stop simpletransbotagi.service

git pull

mv /tmp/best_model/* /root/repos/gitlab.com/remotejob/simpletransbotagi/data/simpletransdata/best_model/
mv /tmp/intent.csv /root/repos/gitlab.com/remotejob/simpletransbotagi/data/simpletransdata/

systemctl start simpletransbotagi.service
journalctl -f -u simpletransbotagi.service

#scp  data/simpletransdata/best_model/* root@159.203.67.26:/root/repos/gitlab.com/remotejob/simpletransbotagi/data/simpletransdata/best_model/
#scp  data/simpletransdata/intent.csv root@159.203.67.26:/root/repos/gitlab.com/remotejob/simpletransbotagi/data/simpletransdata/
python runprod/run.py

python kagglegenerate/simpletransbotagi.py  https://github.com/IDSIA/sacred vs wandb


sqlite3 -header -csv /exwindoz/home/juno/repos/gitlab.com/remotejob/learnpytorch/simpletransdata/mldata.db 'select id,intent from intenttbl;' > data/simpletransdata/intent.csv 

